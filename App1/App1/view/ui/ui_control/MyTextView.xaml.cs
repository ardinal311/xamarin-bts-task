﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UiTextView
    {
        public UiTextView(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
    }
}