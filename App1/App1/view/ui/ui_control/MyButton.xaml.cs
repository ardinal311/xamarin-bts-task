﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyButton
    {
        public MyButton(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
        
        private void ButtonControl_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IToastService>().LongAlert("YOUR MESSAGE");
        }
    }
}