﻿using System;
using System.Collections.Generic;
using System.Linq;
using dotMorten.Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UiAutoComplete
    {
        public UiAutoComplete(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void AutoSuggestBox_TextChanged(object sender, AutoSuggestBoxTextChangedEventArgs e)
        {
            AutoSuggestBox box = (AutoSuggestBox)sender;

            if (e.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                if (string.IsNullOrWhiteSpace(box.Text) || box.Text.Length < 1)
                {
                    box.ItemsSource = null;
                }
                else
                {
                    var suggestion = GetSuggestion(box.Text);
                    box.ItemsSource = suggestion;
                }
            }
        }

        public class CountryOfCity
        {
            public string City { get; set; }

            public string Country { get; set; }

            public string Display => $"{City}, {Country}";
            public override string ToString() => Display;
        }

        private static List<CountryOfCity> GetSuggestion(string text)
        {
            var masterOfCountry = new List<CountryOfCity>
            {
                new CountryOfCity
                {
                    City = "Paris",
                    Country = "France"
                },
                new CountryOfCity
                {
                    City = "PA",
                    Country = "United States"
                },
                new CountryOfCity
                {
                    City = "Parana",
                    Country = "Brazil"
                },
                new CountryOfCity
                {
                    City = "Padua",
                    Country = "Italy"
                },
                new CountryOfCity
                {
                    City = "Pasadena CA",
                    Country = "United States"
                }
            };

            return masterOfCountry.Where(city => city.Display.StartsWith(text, StringComparison.InvariantCultureIgnoreCase)).ToList();
        } 

        private void AutoSuggestBox_QuerySubmitted(object sender, AutoSuggestBoxQuerySubmittedEventArgs e)
        {

        }

        private void AutoSuggestBox_SuggestionChosen(object sender, AutoSuggestBoxSuggestionChosenEventArgs e)
        {

        }
    }
}