﻿using System;
using System.Globalization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyDatePicker
    {
        public MyDatePicker(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
            OpenDatePicker();
        }

        private void OpenDatePicker()
        {
            CustomDatePicker.MinimumDate = new DateTime(1913, 1, 1);
            CustomDatePicker.MaximumDate = new DateTime(2100, 12, 31);
            CustomDatePicker.Date = DateTime.Now;
            CustomDatePicker.IsVisible = false;
        }

        private void BtnDate_Clicked(object sender, EventArgs e)
        {
            CustomDatePicker.Focus();
        }

        private void CustomDatePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            if (DisplayDate != null)
                if (CustomDatePicker != null)
                    DisplayDate.Text = CustomDatePicker.Date.ToString(CultureInfo.InvariantCulture);
        }
    }
}