﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.model.entity;

namespace XamarinTask.view.ui.ui_control.menu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UiControlMenu
    {
        public UiControlMenu(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
            UiControlList.ItemsSource = new List<AndroidMenu>
            {
                new AndroidMenu
                {
                    Tab = 1,
                    Title = "TextView"
                },
                new AndroidMenu
                {
                    Tab = 2,
                    Title = "EditText"
                },
                new AndroidMenu
                {
                    Tab = 3,
                    Title = "AutoCompleteTextView"
                },
                new AndroidMenu
                {
                    Tab = 4,
                    Title = "Button"
                },
                new AndroidMenu
                {
                    Tab = 5,
                    Title = "ImageButton"
                },
                new AndroidMenu
                {
                    Tab = 6,
                    Title = "CheckBox"
                },
                new AndroidMenu
                {
                    Tab = 7,
                    Title = "ToggleButton"
                },
                new AndroidMenu
                {
                    Tab = 8,
                    Title = "RadioButton"
                },
                new AndroidMenu
                {
                    Tab = 9,
                    Title = "RadioGroup"
                },
                new AndroidMenu
                {
                    Tab = 10,
                    Title = "ProgressBar"
                },
                new AndroidMenu
                {
                    Tab = 11,
                    Title = "Spinner"
                },
                new AndroidMenu
                {
                    Tab = 12,
                    Title = "TimePicker"
                },
                new AndroidMenu
                {
                    Tab = 13,
                    Title = "DatePicker"
                },

            };
        }

        private async void UIControlList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is AndroidMenu selected)
                switch (selected.Tab)
                {
                    case 1:
                        await Navigation.PushAsync(new UiTextView(selected.Title));
                        break;
                    case 2:
                        await Navigation.PushAsync(new MyEditText(selected.Title));
                        break;
                    case 3:
                        await Navigation.PushAsync(new UiAutoComplete(selected.Title));
                        break;
                    case 4:
                        await Navigation.PushAsync(new MyButton(selected.Title));
                        break;
                    case 5:
                        await Navigation.PushAsync(new MyImageButton(selected.Title));
                        break;
                    case 6:
                        await Navigation.PushAsync(new MyCheckBox(selected.Title));
                        break;
                    case 7:
                        await Navigation.PushAsync(new MyToggleButton(selected.Title));
                        break;
                    case 8:
                        await Navigation.PushAsync(new MyRadioButton(selected.Title));
                        break;
                    case 9:
                        await Navigation.PushAsync(new MyRadioGroup(selected.Title));
                        break;
                    case 10:
                        await Navigation.PushAsync(new MyProgressBar(selected.Title));
                        break;
                    case 12:
                        await Navigation.PushAsync(new MyTimePicker(selected.Title));
                        break;
                    case 13:
                        await Navigation.PushAsync(new MyDatePicker(selected.Title));
                        break;
                }

            ((ListView)sender).SelectedItem = null;
        }
    }
}