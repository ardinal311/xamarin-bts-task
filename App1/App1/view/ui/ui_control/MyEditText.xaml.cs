﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyEditText
    {
        public MyEditText(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void MyEntry_Focused(object sender, FocusEventArgs e)
        {
            MyEntry.BackgroundColor = Color.LightSkyBlue;
            MyEntry.Opacity = 0.8;
        }

        private void MyButton_Clicked(object sender, EventArgs e)
        {
            var text = MyEntry.Text;
            DependencyService.Get<IToastService>().ShortAlert(text);
        }
    }
}