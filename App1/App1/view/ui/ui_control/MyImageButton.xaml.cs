﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyImageButton
    {
        public MyImageButton(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void CustomImageButton_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IToastService>().LongAlert("Your download is resumed");
        }
    }
}