﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyToggleButton
    {
        public MyToggleButton(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void LeftSwitch_Toggled(object sender, ToggledEventArgs e)
        {
            DependencyService.Get<IToastService>().LongAlert(LeftSwitch.IsToggled
                ? "You have clicked first ON Button --|_"
                : "You have clicked first OFF Button --|_");
        }

        private void RightSwitch_Toggled(object sender, ToggledEventArgs e)
        {
            DependencyService.Get<IToastService>().LongAlert(RightSwitch.IsToggled
                ? "You have clicked second ON Button --|_"
                : "You have clicked second OFF Button --|_");
        }

        private void ButtonControl_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IToastService>().LongAlert("Nice Try!!");
        }
    }
}