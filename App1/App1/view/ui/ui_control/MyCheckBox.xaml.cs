﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyCheckBox
    {
        private static bool like;
        private static bool pain;
        public MyCheckBox(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void LikeCheckBox_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (LikeCheckBox.IsChecked)
            {
                like = true;
            }
            else
            {
                like = false;
            }
        }

        private void PainCheckBox_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if(PainCheckBox.IsChecked)
            {
                pain = true;
            }
            else
            {
                pain = false;
            }
        }

        private void ButtonControl_Clicked(object sender, EventArgs e)
        {
            if (like && pain)
            {
                DependencyService.Get<IToastService>().LongAlert("YOU LIKE XAMARIN FORM AND LOVES THE PAIN");
            }
            if (!like && pain)
            {
                DependencyService.Get<IToastService>().LongAlert("YOU FELT THE PAIN");
            }
            if(like && !pain)
            {
                DependencyService.Get<IToastService>().LongAlert("YOU LIKE XAMARIN FORM AND YOU GOT IT");
            }
            if(!like && !pain)
            {
                DependencyService.Get<IToastService>().LongAlert("YOU BETTER JUST DIE");
            }
        }
    }
}