﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyTimePicker
    {
        public MyTimePicker(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
    }
}