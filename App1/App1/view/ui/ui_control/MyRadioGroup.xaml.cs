﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyRadioGroup
    {
        public MyRadioGroup(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
    }
}