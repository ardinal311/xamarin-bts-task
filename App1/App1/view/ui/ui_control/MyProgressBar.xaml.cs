﻿using System;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms.Xaml;
using XamarinTask.system.tool;

namespace XamarinTask.view.ui.ui_control
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyProgressBar
    {
        public MyProgressBar(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void ShowProgress_Clicked(object sender, EventArgs e)
        {
            Progress();
        }

        async private void Progress()
        {
            var page = new ProgressModal();
            await PopupNavigation.Instance.PushAsync(page);
        }
    }
}