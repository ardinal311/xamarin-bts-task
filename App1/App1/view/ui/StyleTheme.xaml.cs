﻿using System;
using Xamarin.Forms.Xaml;
using XamarinTask.system.tool;

namespace XamarinTask.view.ui
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StyleTheme
    {
        public StyleTheme(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void ThemeBtn_Clicked(object sender, EventArgs e)
        {
            ThemeManager.ChangeTheme(ThemeManager.Themes.Dark);
        }
    }
}