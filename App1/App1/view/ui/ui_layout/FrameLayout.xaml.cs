﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.view.ui.ui_layout
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FrameLayout
    {
        public FrameLayout(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
    }
}