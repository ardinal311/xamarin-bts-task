﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.view.ui.ui_layout
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RelativeLayout
    {
        public RelativeLayout(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
    }
}