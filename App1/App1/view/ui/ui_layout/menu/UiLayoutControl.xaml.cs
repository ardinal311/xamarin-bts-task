﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.model.entity;

namespace XamarinTask.view.ui.ui_layout.menu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListViewUiLayout
    {
        public ListViewUiLayout(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
            UiLayoutList.ItemsSource = new List<AndroidMenu>
            {
                new AndroidMenu
                {
                    Tab = 1,
                    Title = "Linear Layout"
                },
                new AndroidMenu
                {
                    Tab = 2,
                    Title = "Relative Layout"
                },
                new AndroidMenu
                {
                    Tab = 3,
                    Title = "Table Layout"
                },
                new AndroidMenu
                {
                    Tab = 4,
                    Title = "Frame Layout"
                },
                new AndroidMenu
                {
                    Tab = 5,
                    Title = "List View"
                },
                new AndroidMenu
                {
                    Tab = 6,
                    Title = "Grid View"
                },

            };
        }

        private async void UILayoutList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is AndroidMenu selected)
                switch (selected.Tab)
                {
                    case 1:
                        await Navigation.PushAsync(new LinearStackLayout(selected.Title));
                        break;
                    case 2:
                        await Navigation.PushAsync(new RelativeLayout(selected.Title));
                        break;
                    case 3:
                        await Navigation.PushAsync(new TableLayout(selected.Title));
                        break;
                    case 4:
                        await Navigation.PushAsync(new FrameLayout(selected.Title));
                        break;
                }
            ((ListView)sender).SelectedItem = null;
        }
    }
}