﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinTask.view.ui.ui_layout
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LinearStackLayout
    {
        public LinearStackLayout(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void StartServiceClicked(object sender, EventArgs e)
        {
            DisplayAlert("Service Started", "Start Service was clicked", "OK");
        }

        private void PauseServiceClicked(object sender, EventArgs e)
        {
            DisplayAlert("Service Paused", "Pause Service was clicked", "OK");
        }

        private void StopServiceClicked(object sender, EventArgs e)
        {
            DisplayAlert("Service Stopped", "Stop Service was clicked", "OK");
        }

        private void ToggleOrientationClicked(object sender, EventArgs e)
        {
            if (UiStackLayout.Orientation == StackOrientation.Vertical)
            {
                UiStackLayout.Orientation = StackOrientation.Horizontal;
                LabelOrientation.Text = "HORIZONTAL ORIENTATION";
            }
            else
            {
                UiStackLayout.Orientation = StackOrientation.Vertical;
                LabelOrientation.Text = "VERTICAL ORIENTATION";
            }

        }
    }
}