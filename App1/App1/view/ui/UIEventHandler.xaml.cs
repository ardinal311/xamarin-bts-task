﻿using System;
using Xamarin.Forms.Xaml;

namespace XamarinTask.view.ui
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UiEventHandler
    {
        public UiEventHandler(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void SmallBtn_Clicked(object sender, EventArgs e)
        {
            LabelHelloWorld.FontSize = 16;
        }

        private void LargeBtn_Clicked(object sender, EventArgs e)
        {
            LabelHelloWorld.FontSize = 32;
        }
    }
}