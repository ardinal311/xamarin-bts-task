﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.view.basic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Resources
    {
        public Resources(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
    }
}