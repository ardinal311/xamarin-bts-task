﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.view.basic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Architecture
    {
        public Architecture(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
    }
}