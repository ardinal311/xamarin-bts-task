﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.view.useful;

namespace XamarinTask.view.basic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnvironmentSetup
    {
        public EnvironmentSetup(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;

            const string linkAndroidStudio = "https://developer.android.com/studio";
            const string linkSeOracle = "https://www.oracle.com/technetwork/java/javase/downloads/index.html";

            var androidStudioTap = InitializeTapGesture(linkAndroidStudio);
            AndroidStudioLink.GestureRecognizers.Add(androidStudioTap);

            var seOracleTap = InitializeTapGesture(linkSeOracle);
            SeLink.GestureRecognizers.Add(seOracleTap);
        }

        private TapGestureRecognizer InitializeTapGesture(string link)
        {
            var tapGesture = new TapGestureRecognizer();
            tapGesture.Tapped += (s, e) =>
            {
                OpenPage(link);
            };
            return tapGesture;
        }

        private async void OpenPage(string link)
        {
            await Navigation.PushAsync(new OpenWebView("Android Environment Setup", link));
        }
    }
}