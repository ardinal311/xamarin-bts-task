﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.view.basic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ApplicationComponents
    {
        public ApplicationComponents(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
    }
}