﻿using System.Collections.Generic;
using Xamarin.Forms.Xaml;
using XamarinTask.model.entity;

namespace XamarinTask.view.basic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Overview
    {
        public Overview(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;

            ListBasicAndroidFeature.ItemsSource = new List<AndroidBasicOverviewFeatures>
            {
                new AndroidBasicOverviewFeatures
                {
                    No = 1,
                    Title = "Beautiful UI",
                    Desc = "Android OS basic screen provides a beautiful and intuitive user interface."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 2,
                    Title = "Connectivity",
                    Desc = "GSM/EDGE, IDEN, CDMA, EV-DO, UMTS, Bluetooth, Wi-Fi, LTE, NFC and WiMAX."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 3,
                    Title = "Storage",
                    Desc = "SQLite, a lightweight relational database, is used for data storage purposes."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 4,
                    Title = "Media support",
                    Desc = "H.263, H.264, MPEG-4 SP, AMR, AMR-WB, AAC, HE-AAC, AAC 5.1, MP3, MIDI, Ogg Vorbis, WAV, JPEG, PNG, GIF, and BMP."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 5,
                    Title = "Messaging",
                    Desc = "SMS and MMS"
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 6,
                    Title = "Web browser",
                    Desc = "Based on the open-source WebKit layout engine, coupled with Chrome's V8 JavaScript engine supporting HTML5 and CSS3."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 7,
                    Title = "Multi-touch",
                    Desc = "Android has native support for multi-touch which was initially made available in handsets such as the HTC Hero."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 8,
                    Title = "Multi-tasking",
                    Desc = "User can jump from one task to another and same time various application can run simultaneously."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 9,
                    Title = "Resizable widgets",
                    Desc = "Widgets are resizable, so users can expand them to show more content or shrink them to save space."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 10,
                    Title = "Multi-Language",
                    Desc = "Supports single direction and bi-directional text."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 11,
                    Title = "GCM",
                    Desc = "Google Cloud Messaging (GCM) is a service that lets developers send short message data to their users on Android devices, without needing a proprietary sync solution."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 12,
                    Title = "Wi-Fi Direct",
                    Desc = "A technology that lets apps discover and pair directly, over a high-bandwidth peer-to-peer connection."
                },
                new AndroidBasicOverviewFeatures
                {
                    No = 13,
                    Title = "Android Beam",
                    Desc = "A popular NFC-based technology that lets users instantly share, just by touching two NFC-enabled phones together."
                }
            };

            ListBasicApiLevel.ItemsSource = new List<AndroidBasicOverviewApiLevel>
            {
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 6.0",
                    ApiLevel = 23,
                    VersionCode = "MARSHMALLOW"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 5.1",
                    ApiLevel = 22,
                    VersionCode = "LOLLIPOP_MR1"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 5.0",
                    ApiLevel = 21,
                    VersionCode = "LOLLIPOP"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 4.4W",
                    ApiLevel = 20,
                    VersionCode = "KITKAT_WATCH"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 4.4",
                    ApiLevel = 19,
                    VersionCode = "KITKAT"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 4.3",
                    ApiLevel = 18,
                    VersionCode = "JELLY_BEAN_MR2"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 4.2, 4.2.2",
                    ApiLevel = 17,
                    VersionCode = "JELLY_BEAN_MR1"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 4.1, 4.1.1",
                    ApiLevel = 16,
                    VersionCode = "JELLY_BEAN"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 4.0.3, 4.0.4",
                    ApiLevel = 15,
                    VersionCode = "ICE_CREAM_SANDWICH_MR1"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 4.0, 4.0.1, 4.0.2",
                    ApiLevel = 14,
                    VersionCode = "ICE_CREAM_SANDWICH"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 3.2",
                    ApiLevel = 13,
                    VersionCode = "HONEYCOMB_MR2"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 3.1.x",
                    ApiLevel = 12,
                    VersionCode = "HONEYCOMB_MR1"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 3.0.x",
                    ApiLevel = 11,
                    VersionCode = "HONEYCOMB"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 2.3.4, Android 2.3.3",
                    ApiLevel = 10,
                    VersionCode = "GINGERBREAD_MR1"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 2.3.2, Android 2.3.1, Android 2.3",
                    ApiLevel = 9,
                    VersionCode = "GINGERBREAD"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 2.2.x",
                    ApiLevel = 8,
                    VersionCode = "FROYO"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 2.1.x",
                    ApiLevel = 7,
                    VersionCode = "ECLAIR_MR1"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 2.0.1",
                    ApiLevel = 6,
                    VersionCode = "ECLAIR_0_1"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 2.0",
                    ApiLevel = 5,
                    VersionCode = "ECLAIR"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 1.6",
                    ApiLevel = 4,
                    VersionCode = "DONUT"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 1.5",
                    ApiLevel = 3,
                    VersionCode = "CUPCAKE"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 1.1",
                    ApiLevel = 2,
                    VersionCode = "BASE_1_1"
                },
                new AndroidBasicOverviewApiLevel
                {
                    Platform = "Android 1.0",
                    ApiLevel = 1,
                    VersionCode = "BASE"
                }
            };
        }
    }
}