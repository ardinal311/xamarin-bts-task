﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.view.basic
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Home
    {
        public Home(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
    }
}