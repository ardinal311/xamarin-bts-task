﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.advanced
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyLocalNotification
    {
        public MyLocalNotification(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
            //BindingContext = new NotificationViewModel();
        }

        private void NotificationBtn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<INotificationService>().LocalNotification("1", "Notification", "Too Late");
            DependencyService.Get<IToastService>().LongAlert("Clicked");
        }
    }
}