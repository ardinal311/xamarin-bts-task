﻿using System;
using System.Globalization;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.advanced
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LocationServices
    {
        public LocationServices(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        async void LocationBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                var location = await Geolocation.GetLastKnownLocationAsync();

                if(location != null)
                {
                    DependencyService.Get<IToastService>().LongAlert(
                        "Latitude Position : " + location.Latitude.ToString(CultureInfo.InvariantCulture) + "\n Longitude Position : " + location.Longitude.ToString(CultureInfo.InvariantCulture));
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                await DisplayAlert("Failed To Get Location", fnsEx.Message, "OK");
            }
            catch (PermissionException pEx)
            {
                await DisplayAlert("Failed", pEx.Message, "OK");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Failed", ex.Message, "OK");
            }
        }
    }
}