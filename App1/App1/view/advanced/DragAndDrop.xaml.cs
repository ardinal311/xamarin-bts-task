﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.view.advanced
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DragAndDrop
    {
        public DragAndDrop(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }
    }
}