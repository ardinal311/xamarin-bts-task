﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms.Xaml;

namespace XamarinTask.view.advanced
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SendEmail
    {
        public SendEmail(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
            SendBtn.Clicked += SendBtn_Clicked;
        }

        private async void SendBtn_Clicked(object sender, EventArgs e)
        {
            var subject = TxtSubject.Text;
            var recipient = TxtRecipient.Text;
            var body = TxtContent.Text;
            var toSend = new List<string> { recipient };

            await SendTheEmail(toSend, subject, body);
        }

        private async Task SendTheEmail(List<string> recipient, string subject, string msgBody)
        {
            try
            {
                var message = new EmailMessage
                {
                    Subject = subject,
                    Body = msgBody,
                    To = recipient
                };
                await Email.ComposeAsync(message);
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                await DisplayAlert("Failed To Send Message", fnsEx.Message, "OK");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Failed To Send Message", ex.Message, "OK");
            }
        }
    }
}