﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms.Xaml;

namespace XamarinTask.view.advanced
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SendMessage
    {
        public SendMessage(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
            SendBtn.Clicked += SendBtn_Clicked;
        }

        private async void SendBtn_Clicked(object sender, EventArgs e)
        {
            var phoneNumber = TxtPhone.Text;
            var message = TxtMessage.Text;

            if (!string.IsNullOrEmpty(phoneNumber))
            {
                await SendSms(phoneNumber, message);
            }
        }

        private async Task SendSms(string phone, string message)
        {
            try
            {
                var content = new SmsMessage(message, phone);
                await Sms.ComposeAsync(content);
            }
            catch (FeatureNotSupportedException ex)
            {
                await DisplayAlert("Failed", ex.Message, "OK");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Failed", ex.Message, "OK");
            }
        }
    }
}