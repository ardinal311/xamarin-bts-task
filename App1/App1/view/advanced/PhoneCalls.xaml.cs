﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms.Xaml;

namespace XamarinTask.view.advanced
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PhoneCalls
    {
        public PhoneCalls(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
            CallBtn.Clicked += CallBtn_Clicked;
        }

        private async void CallBtn_Clicked(object sender, EventArgs e)
        {
            var number = TxtPhone.Text;
            if(!string.IsNullOrEmpty(number))
            {
                await MakeCall(number);
            }
        }

        private async Task MakeCall(string number)
        {
            try
            {
                PhoneDialer.Open(number);
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                await DisplayAlert("Cannot Make Call", fnsEx.Message, "OK");
            }
            catch (Exception e)
            {
                await DisplayAlert("Cannot Make Call", e.Message, "OK");
            }
        }
    }
}