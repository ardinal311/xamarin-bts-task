﻿using System;
using Plugin.AudioRecorder;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinTask.view.useful
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyRecordingAudio
    {
        private readonly AudioRecorderService _recorder = new AudioRecorderService();
        private readonly AudioPlayer _player = new AudioPlayer();
        public MyRecordingAudio(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
            _recorder.StopRecordingOnSilence = false;
            _player.FinishedPlaying += StopPlayBtn_Clicked;
        }

        private void OnRecordingState()
        {
            RecordBtn.IsEnabled = false;
            PlayBtn.IsEnabled = false;
            StopPlayBtn.IsEnabled = false;
            RecordBtn.BackgroundColor = Color.LightSlateGray;
            PlayBtn.BackgroundColor = Color.LightSlateGray;
            StopPlayBtn.BackgroundColor = Color.LightSlateGray;
        }

        private void OnStopRecordingState()
        {
            RecordBtn.IsEnabled = true;
            PlayBtn.IsEnabled = true;
            StopPlayBtn.IsEnabled = true;
            RecordBtn.BackgroundColor = Color.Indigo;
            PlayBtn.BackgroundColor = Color.Indigo;
            StopPlayBtn.BackgroundColor = Color.Indigo;
        }

        private async void RecordBtn_Clicked(object sender, EventArgs e)
        {
            if (!_recorder.IsRecording)
            {
                await _recorder.StartRecording();
                OnRecordingState();
            }
        }

        private async void StopBtn_Clicked(object sender, EventArgs e)
        {
            if (_recorder.IsRecording)
            {
                await _recorder.StopRecording();
                OnStopRecordingState();
            }
        }

        private async void PlayBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                var filepath = _recorder.GetAudioFilePath();

                if(filepath != null)
                {
                    OnStopRecordingState();
                    _player.Play(filepath);
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Failed", ex.Message, "OK");
            }
        }

        private void StopPlayBtn_Clicked(object sender, EventArgs e)
        {
            OnStopRecordingState();
        }
    }
}