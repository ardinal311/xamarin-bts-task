﻿using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using Map = Xamarin.Forms.Maps.Map;

namespace XamarinTask.view.useful
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyMap
    {
        public MyMap(string selectedTitle)
        {
            InitializeComponent();
            ApplicationTitle.Title = selectedTitle;

            var latitude = GetCurrentLocation().Result.Latitude;
            var longitude = GetCurrentLocation().Result.Longitude;

            var map = new Map(MapSpan.FromCenterAndRadius(center: new Position(latitude, longitude), radius: Distance.FromMiles(0.3))) ;
            map.IsShowingUser = true;
            map.HeightRequest = 100;
            map.WidthRequest = 960;
            map.VerticalOptions = LayoutOptions.FillAndExpand;

            var stack = new StackLayout {Spacing = 0};
            stack.Children.Add(map);
            Content = stack;
        }

        private async Task<Location> GetCurrentLocation()
        {
            var location = await Geolocation.GetLastKnownLocationAsync();
            return location; 
        }
    }
}