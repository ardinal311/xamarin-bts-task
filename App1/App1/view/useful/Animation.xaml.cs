﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinTask.view.useful
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Animation
    {
        private bool _state;
        private double _opacity;
        public Animation(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private async void BtnZoomer_Clicked(object sender, EventArgs e)
        {
            if(!_state)
            {
                await XImage.ScaleTo(3, 2000, Easing.CubicIn);
                _state = true;
            }
            else
            {
                await XImage.ScaleTo(1, 2000, Easing.CubicOut);
                _state = false;
            }
        }

        private async void BtnClockwise_Clicked(object sender, EventArgs e)
        {
            if(!_state)
            {
                await XImage.RotateTo(360, 2000, Easing.SinOut);
                _state = true;
            }
            else
            {
                await XImage.RotateTo(0, 2000, Easing.SinOut);
                _state = false;
            }
        }

        private async void BtnFader_Clicked(object sender, EventArgs e)
        {
            if (!_state)
            {
                await XImage.FadeTo(_opacity, 2000, Easing.Linear);
                _opacity = 0;
                _state = true;
            }
            else
            {
                await XImage.FadeTo(_opacity, 2000, Easing.Linear);
                _opacity = 1;
                _state = false;
            }
        }

        private async void BtnBlinker_Clicked(object sender, EventArgs e)
        {
            await XImage.FadeTo(0, 2000, Easing.Linear);
            await XImage.FadeTo(1, 2000, Easing.Linear);
        }

        private async void BtnMover_Clicked(object sender, EventArgs e)
        {
            if(!_state)
            {
                await XImage.TranslateTo(100, 0, 2000, Easing.SinOut);
                _state = true;
            }
            else
            {
                await XImage.TranslateTo(0, 0, 2000, Easing.SinInOut);
                _state = false;
            }
        }

        private async void BtnSlider_Clicked(object sender, EventArgs e)
        {
            if(!_state)
            {
                XImage.TranslationY = -XImage.Height;
                await XImage.TranslateTo(0, 0, 1000);
            }
        }
    }
}