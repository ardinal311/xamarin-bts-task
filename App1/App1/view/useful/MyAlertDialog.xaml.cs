﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.useful
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyAlertDialog
    {
        public MyAlertDialog(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private async void AlertButton_Clicked(object sender, EventArgs e)
        {
            var answer = await DisplayAlert("", "Are you sure, You wanted to make decision", "Yes", "Cancel");
            if (answer)
            {
                DependencyService.Get<IToastService>().ShortAlert("You clicked yes button");
            }
            else
            {
                DependencyService.Get<IToastService>().LongAlert("You clicked cancel button");
            }
        }
    }
}