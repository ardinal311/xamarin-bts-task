﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinTask.view.useful
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OpenWebView
    {
        public OpenWebView(string title, string url)
        {
            InitializeComponent();
            WebView.Source = url;
            AndroidBasicTitle.Title = title;
        }

        void WebviewNavigating(object sender, WebNavigatingEventArgs e)
        {
            Loading.IsVisible = true;
            Loading.IsRunning = true;
            Loading.IsEnabled = true;
            WebView.IsVisible = false;
        }

        void WebviewNavigated(object sender, WebNavigatedEventArgs e)
        {
            Loading.IsVisible = false;
            Loading.IsRunning = false;
            Loading.IsEnabled = false;
            WebView.IsVisible = true;
        }
    }
}