﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.useful
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyBluetooth
    {
        
        public MyBluetooth(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void TurnOnBtn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IBluetoothService>().TurnOnBluetooth();
        }

        private void GetVisibleBtn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IBluetoothService>().GetBluetoothVisibility();
        }

        private void ListDeviceBtn_Clicked(object sender, EventArgs e)
        {
            var devices = DependencyService.Get<IBluetoothService>().GetPairedDevices();
            BluetoothListView.ItemsSource = devices;
        }

        private void TurnOffBtn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IBluetoothService>().TurnOffBluetooth();
        }
    }
}