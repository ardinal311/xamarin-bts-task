﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.useful
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyAudioManager
    {
        public MyAudioManager(string title)
        {
            InitializeComponent();
            ApplicationTitle.Title = title;
        }

        private void ModeBtn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IRingerService>().GetMode();
        }

        private void RingBtn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IRingerService>().OnRinging();
        }

        private void VibrateBtn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IRingerService>().OnVibrate();
        }

        private void SilentBtn_Clicked(object sender, EventArgs e)
        {
            DependencyService.Get<IRingerService>().OnSilent();
        }
    }
}