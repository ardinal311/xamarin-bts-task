﻿using System;
using Plugin.Battery;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.system.interfaces;

namespace XamarinTask.view.useful
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyBestPractice
    {
        public MyBestPractice(string selectedTitle)
        {
            InitializeComponent();
            ApplicatiionTitle.Title = selectedTitle;
        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            if (CrossBattery.IsSupported)
            {
                var falta = CrossBattery.Current.Status;
                DependencyService.Get<IToastService>().ShortAlert(falta.ToString());
            }
        }
    }
}