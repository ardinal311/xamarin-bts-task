﻿namespace XamarinTask.model.entity
{
    internal class AndroidBasicOverviewFeatures
    {
        public int No { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
    }
}
