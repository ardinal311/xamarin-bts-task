﻿namespace XamarinTask.model.entity
{
    internal class AndroidBasicOverviewApiLevel
    {
        public string Platform { get; set; }
        public int ApiLevel { get; set; }
        public string VersionCode { get; set; }
    }
}
