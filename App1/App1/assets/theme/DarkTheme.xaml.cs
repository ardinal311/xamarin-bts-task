﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.assets.theme
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DarkTheme
    {
        public DarkTheme()
        {
            InitializeComponent();
        }
    }
}