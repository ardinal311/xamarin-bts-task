﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.assets.theme
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LightTheme
    {
        public LightTheme()
        {
            InitializeComponent();
        }
    }
}