﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.model.entity;
using XamarinTask.view.ui.ui_control;
using XamarinTask.view.useful;
using Animation = XamarinTask.view.useful.Animation;

namespace XamarinTask.menu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UsefulMenu
    {
        public UsefulMenu()
        {
            InitializeComponent();
            ApplicationTitle.Title = "Android Useful Examples";
            ListUsefulMenu.ItemsSource = new List<AndroidMenu>
            {
                new AndroidMenu
                {
                    Tab = 1,
                    Title = "Android - Alert Dialog"
                },
                new AndroidMenu
                {
                    Tab = 2,
                    Title = "Android - Animations"
                },
                new AndroidMenu
                {
                    Tab = 3,
                    Title = "Android - Audio Capture"
                },
                new AndroidMenu
                {
                    Tab = 4,
                    Title = "Android - Audio Manager"
                },
                new AndroidMenu
                {
                    Tab = 5,
                    Title = "Android - Auto Complete"
                },
                new AndroidMenu
                {
                    Tab = 6,
                    Title = "Android - Best Practices"
                },
                new AndroidMenu
                {
                    Tab = 7,
                    Title = "Android - Bluetooth"
                },
                new AndroidMenu
                {
                    Tab = 8,
                    Title = "Android - Camera"
                },
                new AndroidMenu
                {
                    Tab = 9,
                    Title = "Android - Clipboard"
                },
                new AndroidMenu
                {
                    Tab = 10,
                    Title = "Android - Custom Fonts"
                },
                new AndroidMenu
                {
                    Tab = 11,
                    Title = "Android - Data Backup"
                },
                new AndroidMenu
                {
                    Tab = 12,
                    Title = "Android - Developer Tools"
                },
                new AndroidMenu
                {
                    Tab = 13,
                    Title = "Android - Emulator"
                },
                new AndroidMenu
                {
                    Tab = 14,
                    Title = "Android - Facebook Integration"
                },
                new AndroidMenu
                {
                    Tab = 15,
                    Title = "Android - Gestures"
                },
                new AndroidMenu
                {
                    Tab = 16,
                    Title = "Android - Google Maps"
                },
                new AndroidMenu
                {
                    Tab = 17,
                    Title = "Android - Image Effects"
                },
                new AndroidMenu
                {
                    Tab = 18,
                    Title = "Android - Image Switcher"
                },
                new AndroidMenu
                {
                    Tab = 19,
                    Title = "Android - Internal Storage"
                },
                new AndroidMenu
                {
                    Tab = 20,
                    Title = "Android - JetPlayer"
                },
                new AndroidMenu
                {
                    Tab = 21,
                    Title = "Android - JSON Parser"
                }
            };
        }
        private async void ListUsefulMenu_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is AndroidMenu selected)
                switch (selected.Tab)
                {
                    case 1:
                        await Navigation.PushAsync(new MyAlertDialog(selected.Title));
                        break;
                    case 2:
                        await Navigation.PushAsync(new Animation(selected.Title));
                        break;
                    case 3:
                        await Navigation.PushAsync(new MyRecordingAudio(selected.Title));
                        break;
                    case 4:
                        await Navigation.PushAsync(new MyAudioManager(selected.Title));
                        break;
                    case 5:
                        await Navigation.PushAsync(new UiAutoComplete(selected.Title));
                        break;
                    case 6:
                        await Navigation.PushAsync(new MyBestPractice(selected.Title));
                        break;
                    case 7:
                        await Navigation.PushAsync(new MyBluetooth(selected.Title));
                        break;
                    case 8:
                        await Navigation.PushAsync(new MyCamera(selected.Title));
                        break;
                    case 16:
                        await Navigation.PushAsync(new MyMap(selected.Title));
                        break;
                    default:
                        await Navigation.PushAsync(new MyAlertDialog(selected.Title));
                        break;
                }

            ((ListView)sender).SelectedItem = null;
        }
    }
}