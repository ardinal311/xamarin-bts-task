﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.model.entity;
using XamarinTask.view.basic;

namespace XamarinTask.menu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class BasicMenu
    {
        public BasicMenu()
        {
            InitializeComponent();
            ApplicationTitle.Title = "Android Basic";
            ListBasicMenu.ItemsSource = new List<AndroidMenu>
            {
                new AndroidMenu
                {
                    Tab = 1,
                    Title = "Android - Home"
                },
                new AndroidMenu
                {
                    Tab = 2,
                    Title = "Android - Overview"
                },
                new AndroidMenu
                {
                    Tab = 3,
                    Title = "Android - Environment Setup"
                },
                new AndroidMenu
                {
                    Tab = 4,
                    Title = "Android - Architecture"
                },
                new AndroidMenu
                {
                    Tab = 5,
                    Title = "Android - Application Components"
                },
                new AndroidMenu
                {
                    Tab = 6,
                    Title = "Android - Hello World Example"
                },
                new AndroidMenu
                {
                    Tab = 7,
                    Title = "Android - Resources"
                },
                new AndroidMenu
                {
                    Tab = 8,
                    Title = "Android - Activities"
                },
                new AndroidMenu
                {
                    Tab = 9,
                    Title = "Android - Services"
                },
                new AndroidMenu
                {
                    Tab = 10,
                    Title = "Android - Broadcast Receivers"
                },
                new AndroidMenu
                {
                    Tab = 11,
                    Title = "Android - Content Providers"
                },
                new AndroidMenu
                {
                    Tab = 12,
                    Title = "Android - Fragments"
                },
                new AndroidMenu
                {
                    Tab = 13,
                    Title = "Android - Intents/Filters"
                }
            };
        }
       
        private async void ListBasicMenu_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is AndroidMenu selected)
                switch (selected.Tab)
                {
                    case 1:
                        await Navigation.PushAsync(new Home(selected.Title));
                        break;
                    case 2:
                        await Navigation.PushAsync(new Overview(selected.Title));
                        break;
                    case 3:
                        await Navigation.PushAsync(new EnvironmentSetup(selected.Title));
                        break;
                    case 4:
                        await Navigation.PushAsync(new Architecture(selected.Title));
                        break;
                    case 5:
                        await Navigation.PushAsync(new ApplicationComponents(selected.Title));
                        break;
                    case 6:
                        await Navigation.PushAsync(new HelloWorldExample(selected.Title));
                        break;
                    case 7:
                        await Navigation.PushAsync(new Resources(selected.Title));
                        break;
                    default:
                        await Navigation.PushAsync(new Home(selected.Title));
                        break;
                }

            ((ListView)sender).SelectedItem = null;
        }
    }
}