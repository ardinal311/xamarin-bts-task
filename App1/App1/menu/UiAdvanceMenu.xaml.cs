﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTask.model.entity;
using XamarinTask.view.ui;
using XamarinTask.view.ui.ui_control.menu;
using XamarinTask.view.ui.ui_layout.menu;

namespace XamarinTask.menu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UiAdvanceMenu
    {
        public UiAdvanceMenu()
        {
            InitializeComponent();
            ApplicationTitle.Title = "Android User Interface &amp; Advanced Concepts";
            ListUiMenu.ItemsSource = new List<AndroidMenu>
            {
                new AndroidMenu
                {
                    Tab = 1,
                    Title = "Android - UI Layouts"
                },
                new AndroidMenu
                {
                    Tab = 2,
                    Title = "Android - UI Controls"
                },
                new AndroidMenu
                {
                    Tab = 3,
                    Title = "Android - Event Handling"
                },
                new AndroidMenu
                {
                    Tab = 4,
                    Title = "Android - Styles and Themes"
                },
                new AndroidMenu
                {
                    Tab = 5,
                    Title = "Android - Custom Components"
                }
            };

            ListAdvanceMenu.ItemsSource = new List<AndroidMenu>
            {
                new AndroidMenu
                {
                    Tab = 1,
                    Title = "Android - Drag And Drop"
                },
                new AndroidMenu
                {
                    Tab = 2,
                    Title = "Android - Notifications"
                },
                new AndroidMenu
                {
                    Tab = 3,
                    Title = "Android - Location Based Services"
                },
                new AndroidMenu
                {
                    Tab = 4,
                    Title = "Android - Sending Email"
                },
                 new AndroidMenu
                {
                    Tab = 5,
                    Title = "Android - Sending Message"
                },
                new AndroidMenu
                {
                    Tab = 6,
                    Title = "Android - Phone Calls"
                },
                new AndroidMenu
                {
                    Tab = 7,
                    Title = "Publishing Android Application"
                }
            };
        }
        private async void ListUiMenu_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is AndroidMenu selected)
                switch (selected.Tab)
                {
                    case 1:
                        await Navigation.PushAsync(new ListViewUiLayout(selected.Title));
                        break;
                    case 2:
                        await Navigation.PushAsync(new UiControlMenu(selected.Title));
                        break;
                    case 3:
                        await Navigation.PushAsync(new UiEventHandler(selected.Title));
                        break;
                    case 4:
                        await Navigation.PushAsync(new StyleTheme(selected.Title));
                        break;
                    default:
                        await Navigation.PushAsync(new ListViewUiLayout(selected.Title));
                        break;
                }

            ((ListView)sender).SelectedItem = null;
        }

        private async void ListAdvanceMenu_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item is AndroidMenu selectedIndex)
                switch (selectedIndex.Tab)
                {
                    case 1:
                        await Navigation.PushAsync(new view.advanced.DragAndDrop(selectedIndex.Title));
                        break;
                    case 2:
                        await Navigation.PushAsync(new view.advanced.MyLocalNotification(selectedIndex.Title));
                        break;
                    case 3:
                        await Navigation.PushAsync(new view.advanced.LocationServices(selectedIndex.Title));
                        break;
                    case 4:
                        await Navigation.PushAsync(new view.advanced.SendEmail(selectedIndex.Title));
                        break;
                    case 5:
                        await Navigation.PushAsync(new view.advanced.SendMessage(selectedIndex.Title));
                        break;
                    case 6:
                        await Navigation.PushAsync(new view.advanced.PhoneCalls(selectedIndex.Title));
                        break;
                    default:
                        await Navigation.PushAsync(new view.advanced.DragAndDrop(selectedIndex.Title));
                        break;
                }

            ((ListView)sender).SelectedItem = null;
        }
    }
}