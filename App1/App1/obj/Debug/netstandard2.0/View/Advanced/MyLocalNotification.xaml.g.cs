//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Xamarin.Forms.Xaml.XamlResourceIdAttribute("XamarinTask.view.advanced.MyLocalNotification.xaml", "view/advanced/MyLocalNotification.xaml", typeof(global::XamarinTask.view.advanced.MyLocalNotification))]

namespace XamarinTask.view.advanced {
    
    
    [global::Xamarin.Forms.Xaml.XamlFilePathAttribute("view\\advanced\\MyLocalNotification.xaml")]
    public partial class MyLocalNotification : global::Xamarin.Forms.ContentPage {
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private global::Xamarin.Forms.ContentPage ApplicationTitle;
        
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "2.0.0.0")]
        private void InitializeComponent() {
            global::Xamarin.Forms.Xaml.Extensions.LoadFromXaml(this, typeof(MyLocalNotification));
            ApplicationTitle = global::Xamarin.Forms.NameScopeExtensions.FindByName<global::Xamarin.Forms.ContentPage>(this, "ApplicationTitle");
        }
    }
}
