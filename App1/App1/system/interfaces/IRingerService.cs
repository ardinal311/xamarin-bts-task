﻿namespace XamarinTask.system.interfaces
{
    public interface IRingerService
    {
        void OnSilent();
        void OnRinging();
        void OnVibrate();
        void GetMode();
    }
}
