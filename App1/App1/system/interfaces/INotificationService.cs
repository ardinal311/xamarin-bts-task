﻿namespace XamarinTask.system.interfaces
{
    public interface INotificationService
    {
        void LocalNotification(string notifyId, string title, string body);
    }
}
