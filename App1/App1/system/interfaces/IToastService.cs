﻿namespace XamarinTask.system.interfaces
{
    public interface IToastService
    {
      void LongAlert(string message);
      void ShortAlert(string message);
    }
}
