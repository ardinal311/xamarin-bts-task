﻿using System.Collections.Generic;
using Android.Bluetooth;

namespace XamarinTask.system.interfaces
{
    public interface IBluetoothService
    {
        void TurnOnBluetooth();
        void TurnOffBluetooth();
        IEnumerable<BluetoothDevice> GetPairedDevices();
        void GetBluetoothVisibility();
    }
}
