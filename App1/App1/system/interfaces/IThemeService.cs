﻿using XamarinTask.system.tool;

namespace XamarinTask.system.interfaces
{
    public interface IThemeService
    {
        void OnThemeChanged(ThemeManager.Themes theme);
    }
}