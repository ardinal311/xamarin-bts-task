﻿using System;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinTask.system.tool
{
    [ContentProperty(nameof(Source))]
    public class ImageHandler : IMarkupExtension
    {
        public string Source { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Source == null)
            {
                return null;
            }

            var imageSource = ImageSource.FromResource(Source, typeof(ImageHandler).GetTypeInfo().Assembly);
            return imageSource;
        }
    }
}
