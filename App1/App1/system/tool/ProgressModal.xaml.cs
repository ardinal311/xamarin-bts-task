﻿using Xamarin.Forms.Xaml;

namespace XamarinTask.system.tool
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProgressModal
    {
        public ProgressModal()
        {
            InitializeComponent();
            Animation = new Rg.Plugins.Popup.Animations.ScaleAnimation();
        }
    }
}