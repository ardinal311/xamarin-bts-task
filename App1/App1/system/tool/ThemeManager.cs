﻿using Plugin.Settings;
using Xamarin.Forms;

namespace XamarinTask.system.tool
{
    public class ThemeManager
    {
        public enum Themes
        {
            Light,
            Dark
        }
        public static void ChangeTheme(Themes theme)
        {
            var mergedDictionaries = Application.Current.Resources.MergedDictionaries;
            if (mergedDictionaries != null)
            {
                mergedDictionaries.Clear();

                CrossSettings.Current.AddOrUpdateValue("SelectedTheme", (int)theme);

                switch (theme)
                {
                    case Themes.Light:
                        {
                            mergedDictionaries.Add(new assets.theme.LightTheme());
                            break;
                        }
                    case Themes.Dark:
                        {
                            mergedDictionaries.Add(new assets.theme.DarkTheme());
                            break;
                        }
                    default:
                        mergedDictionaries.Add(new assets.theme.LightTheme());
                        break;
                }
            }
        }

        public static void LoadTheme()
        {
            Themes currentTheme = CurrentTheme();
            ChangeTheme(currentTheme);
        }

        public static Themes CurrentTheme()
        {
            return (Themes)CrossSettings.Current.GetValueOrDefault("SelectedTheme", (int)Themes.Light);
        }
    }
}