﻿using Android.Widget;
using App1.Droid;
using Xamarin.Forms;
using XamarinTask.system.interfaces;

[assembly: Dependency(typeof(MyToastService))]
namespace App1.Droid
{
    class MyToastService : IToastService
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Android.App.Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Android.App.Application.Context, message, ToastLength.Short).Show();
        }
    }
}