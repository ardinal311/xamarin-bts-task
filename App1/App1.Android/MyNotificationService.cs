﻿using Android.App;
using Android.Content;
using Android.Support.V4.App;
using App1.Droid;
using Android.OS;
using App1.Droid.Resources;
using XamarinTask.system.interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(MyNotificationService))]
namespace App1.Droid
{
    internal class MyNotificationService : INotificationService
    {
        private const int NotificationId = 1000;
        private const string ChannelId = "location_notification";
        public static readonly string CountKey = "count";
        private static NotificationManager Manager => (NotificationManager)Application.Context.GetSystemService(Context.NotificationService);
        public MyNotificationService()
        {
            CreateNotificationChannel();
        }

        private static void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                return;
            }

            var channelName = new Java.Lang.String("MY_CHANNEL");
            const string channelDescription = "MY_DESCRIPTION";

            var channel = new NotificationChannel(ChannelId, channelName, NotificationImportance.Default)
            {
                Description = channelDescription
            };

            Manager.CreateNotificationChannel(channel);
        }

        public void LocalNotification(string notifyId, string title, string body)
        {
            var toast = new MyToastService();
            toast.LongAlert("DATA BOSS MANA" + notifyId + title + body);

            var builder = new NotificationCompat.Builder(Application.Context, ChannelId)
                .SetAutoCancel(true)
                .SetContentTitle(title)
                .SetContentText(body)
                .SetSmallIcon(Resource.Drawable.abc_ab_share_pack_mtrl_alpha);

            var notificationManager = NotificationManagerCompat.From(Application.Context);
            notificationManager.Notify(NotificationId, builder.Build());

            //NotificationCompat.Builder builder = new NotificationCompat.Builder(Application.Context)
            //    .SetContentTitle(title)
            //    .SetSmallIcon(Resource.Drawable.abc_ic_star_black_36dp)
            //    .SetContentText(body);

            //NotificationManagerCompat notifMgr = NotificationManagerCompat.From(Application.Context);
            //notifMgr.Notify(_randomNumber, builder.Build());
        }
    }
}