﻿using System;

using Android.App;
using Android.Content;
using Android.Media;
using App1.Droid;
using XamarinTask.system.interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(MyRingerService))]
namespace App1.Droid
{
    class MyRingerService : IRingerService
    {
        private readonly AudioManager _am;
        private readonly NotificationManager _nm;

        public MyRingerService()
        {
            _am = (AudioManager)Application.Context.GetSystemService(Context.AudioService);
            _nm = (NotificationManager)Application.Context.GetSystemService(Context.NotificationService);
        }

        public void OnRinging()
        {
            try
            {
                var dndStatus = _nm.CurrentInterruptionFilter;

                if (dndStatus == InterruptionFilter.None)
                {
                    TurnOffDnd();
                }
                _am.RingerMode = RingerMode.Normal;
                showToast("Now In Ringing Mode", "short");
            }
            catch (Exception ex)
            {
                showToast(ex.Message, "short");
            }

        }

        public void OnSilent()
        {
            try
            {
                var dndStatus = _nm.CurrentInterruptionFilter;

                if (dndStatus == InterruptionFilter.None)
                {
                    TurnOffDnd();
                }

                _am.RingerMode = RingerMode.Silent;
                showToast("Now In Silent Mode", "short");
            }
            catch (Exception ex)
            {
                showToast(ex.Message, "short");
            }
        }

        public void OnVibrate()
        {
            try
            {
                var dndStatus = _nm.CurrentInterruptionFilter;

                if (dndStatus == InterruptionFilter.None)
                {
                    TurnOffDnd();
                }

                _am.RingerMode = RingerMode.Vibrate;
                showToast("Now In Vibrate Mode", "short");
            }
            catch (Exception ex)
            {
                showToast(ex.Message, "short");
            }
        }

        public void GetMode()
        {
            var myMode = _am.RingerMode;

            if (myMode == RingerMode.Normal)
            {
                showToast("Now In Ringing Mode", "short");
            }
            else if (myMode == RingerMode.Vibrate)
            {
                showToast("Now In Vibrate Mode", "short");
            }
            else
            {
                showToast("Now In Silent Mode", "short");
            }
        }

        private void showToast(string message, string length)
        {
            var toaster = new MyToastService();
            if (length == "long")
            {
                toaster.LongAlert(message);
            }
            if (length == "short")
            {
                toaster.ShortAlert(message);
            }

        }

        private void TurnOffDnd()
        {
            _nm.SetInterruptionFilter(InterruptionFilter.All);
        }
    }
}