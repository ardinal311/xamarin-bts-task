﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Support.V7.App;
using App1.Droid;
using XamarinTask.system.interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(MyBluetoothService))]
namespace App1.Droid
{
    internal class MyBluetoothService : IBluetoothService
    {
        private readonly BluetoothAdapter _ba;
        private ICollection<BluetoothDevice> _pairedDevices;
        private readonly MyToastService _toast;
        private readonly AppCompatActivity _activity;
        private static int RequestEnableBt { get; } = 2;

        public MyBluetoothService()
        {
            _ba = BluetoothAdapter.DefaultAdapter;
            _toast = new MyToastService();
            _activity = (AppCompatActivity)Application.Context;
        }

        public void GetBluetoothVisibility()
        {
            Intent visible = new Intent(BluetoothAdapter.ActionRequestDiscoverable);
            _activity.StartActivityForResult(visible, RequestEnableBt);
        }

        public IEnumerable<BluetoothDevice> GetPairedDevices()
        {
            _pairedDevices = _ba.BondedDevices;
            return _pairedDevices.ToList();
        }

        public void TurnOffBluetooth()
        {
            _ba.Disable();
            _toast.ShortAlert("Bluetooth Turned Off");
        }

        public void TurnOnBluetooth()
        {
            if (!_ba.IsEnabled)
            {
                Intent intent = new Intent(BluetoothAdapter.ActionRequestEnable);
                _activity.StartActivityForResult(intent, RequestEnableBt);
                _toast.ShortAlert("Bluetooth Turned ON");
            }
            else
            {
                _toast.ShortAlert("Bluetooth Already On");
            }
        }
    }
}