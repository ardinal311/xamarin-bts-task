﻿using Android.Content;
using Android.Preferences;

namespace App1.Droid
{
    public class MyPreferenceService
    {
        private ISharedPreferences mSharedPreferences;
        private ISharedPreferencesEditor mPreferencesEditor;

        private static string PREFERENCE_ACCESS_KEY = "PREFERENCE_ACCESS_KEY";

        public MyPreferenceService(Context context)
        {
            mSharedPreferences = PreferenceManager.GetDefaultSharedPreferences(context);
            mPreferencesEditor = mSharedPreferences.Edit();
        }

        public void SaveAccessKey(string key, bool allowed)
        {
            mPreferencesEditor.PutString(PREFERENCE_ACCESS_KEY, key);
            mPreferencesEditor.PutBoolean(PREFERENCE_ACCESS_KEY, allowed);
            mPreferencesEditor.Commit();
        }

        public bool GetAccessKey(string key)
        {
            return mSharedPreferences.GetBoolean(key, false);
        }
    }
}